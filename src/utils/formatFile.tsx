import {
  VideoCameraTwoTone,
  FileImageTwoTone,
  FileExcelTwoTone,
  FileWordTwoTone,
  FilePdfTwoTone,
  FilePptTwoTone,
  FileZipTwoTone,
  FileTextTwoTone
} from '@ant-design/icons'
import React from 'react'

export type ContentType = 'pdf' | 'image' | 'text' | 'jpg' | ''
export const CONTENT_TYPE: ['pdf', 'image', 'text', 'jpg', ''] = ['pdf', 'image', 'text', 'jpg', '']

export const getContentTypeIcon = (content_type?: string) => {
  switch (content_type) {
    case 'video/mp4':
    case 'video/mpeg':
    case 'video/x-msvideo':
      return <VideoCameraTwoTone twoToneColor='#4f41cc' />
    case 'image/gif':
    case 'image/png':
    case 'image/jpeg':
    case 'image/bmp':
      return <FileImageTwoTone twoToneColor='#eacc62' />
    case 'jpg':
      return <FileImageTwoTone twoToneColor='#eacc62' />
    case 'png':
      return <FileImageTwoTone twoToneColor='#eacc62' />
    case 'image':
      return <FileImageTwoTone twoToneColor='#eacc62' />
    case 'application/vnd.ms-excel':
    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
    case 'text/csv':
      return <FileExcelTwoTone twoToneColor='#1baf60' />
    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
    case 'application/msword':
      return <FileWordTwoTone twoToneColor='#3653c5' />
    case 'application/pdf':
      return <FilePdfTwoTone twoToneColor='#ec0908' />
    case 'pdf':
      return <FilePdfTwoTone twoToneColor='#ec0908' />
    case 'application/vnd.ms-powerpoint':
    case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
      return <FilePptTwoTone twoToneColor='#dc6746' />
    case 'application/zip':
    case 'application/vnd.rar':
      return <FileZipTwoTone twoToneColor='#d765e5' />
    default:
      return <FileTextTwoTone />
  }
}

export const fileCategorize = (constent_type: string) => {
  let type: ContentType = ''
  for (const _type of CONTENT_TYPE) {
    if (constent_type.includes(_type)) {
      type = _type
      break
    }
  }
  return type
}
