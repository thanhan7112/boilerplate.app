import { mergeDeepWith } from 'ramda'
import { Axis, Legend, Title } from 'vega'
import { AutoDimensionCalculator, DimensionCalculator, GraphTextBuilder } from '~/lib/vega/model-graph'
import { PermissionItemProps } from '~/model'

export const findPermissionByUrl = (url: string, permissions?: PermissionItemProps[]) => {
  let result: PermissionItemProps | undefined = undefined
  const p = permissions?.find((item) => item.url === url)
  if (p) {
    result = p
  } else {
    permissions?.map((item) => {
      result = findPermissionByUrl(url, item.children) || result
      // return result
    })
  }
  return result
}
export const findPermissionByCode = (permissionContext: string, permissions?: PermissionItemProps[]) => {
  let result: PermissionItemProps | undefined = undefined
  const p = permissions?.find((item) => item.permissionContext === permissionContext)
  if (p) {
    result = p
  } else {
    permissions?.map((item) => {
      result = findPermissionByCode(permissionContext, item.children) || result
    })
  }
  return result
}

export const findPermissionById = (id: number, permissions?: PermissionItemProps[]) => {
  let result: PermissionItemProps | undefined = undefined
  const p = permissions?.find((item) => item.id === id)
  if (p) {
    result = p
  } else {
    permissions?.map((item) => {
      result = findPermissionById(id, item.children) || result
      return result
    })
  }
  return result
}

// Hàm tìm kiếm node trong cây dựa trên id
export function findNodeById(id: number, list: PermissionItemProps[]): PermissionItemProps | null {
  let result: PermissionItemProps | null = null

  if (list.length === 0) {
    return null
  }

  const stack = (node: PermissionItemProps) => {
    if (node.id === id) {
      return node
    }

    // Duyệt qua các node con nếu có
    if (node.children && node.children.length > 0) {
      for (const child of node.children) {
        const foundNode = stack(child)
        if (foundNode) {
          result = foundNode
        }
      }
    }
  }

  for (const child of list) {
    stack(child)
  }

  // Trả về null nếu không tìm thấy
  return result
}

export function mergeWithDefault<D = Record<string, any>>(defaultObject: Partial<D> = {}, object: Partial<D> = {}): D {
  const mergeCriteria = (left: any, right: any) => (right === undefined || right === null ? left : right)

  return mergeDeepWith(mergeCriteria, defaultObject, object)
}

export const getDefaultSize = (containerSize?: {
  height?: any
  width?: any
  minHeight?: any
  maxHeight?: any
  maxWidth?: any
  minWidth?: any
  heightByDataRatio?: any
  widthByDataRatio?: any
}) => {
  return {
    height: (() => 'auto') as AutoDimensionCalculator,
    width: (() => 'auto') as AutoDimensionCalculator,
    minHeight: (() => 0) as DimensionCalculator,
    maxHeight:
      containerSize?.heightByDataRatio || containerSize?.height
        ? ((() => 10000) as DimensionCalculator)
        : ((() => 400) as DimensionCalculator),
    maxWidth: containerSize?.width ? ((() => 10000) as DimensionCalculator) : ((() => 2000) as DimensionCalculator),
    minWidth: (() => 0) as DimensionCalculator,
    heightByDataRatio: null as number | null,
    widthByDataRatio: null as number | null,
    maxConfinedHeight: null as DimensionCalculator | null
  }
}

export const defaultFont =
  '"Roboto Condensed",Roboto,-apple-system,BlinkMacSystemFont,"Segoe UI","Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"'

export const defaultTitle: (font?: string) => Partial<Title> = (font = defaultFont) => ({
  anchor: 'start',
  frame: 'group',
  encode: {
    enter: {
      fontSize: { value: 45 },
      fill: { value: 'transparent' },
      fontWeight: { value: 'normal' },
      font: { value: font }
    },
    update: {
      y: { signal: '15', offset: -15 }
    }
  }
})

export function mergeClass(...args: (string | undefined | null)[]) {
  return args.filter(Boolean).join(' ')
}

const PostFillColorMap: Record<string, string> = {}
export const withPostFillColor = (data: Record<string, any>[], dataField: string, colorField?: string) => {
  if (!colorField) return data
  const colorList = ['#222222', '#282828', '#333333', '#383838', '#444444', '#484848', '#555555', '#585858']
  let currentColorListIndex = 0
  const defaultColor = '#000000'

  return data.map((entry) => {
    if (entry?.[colorField] != null) return entry
    const key = entry?.[dataField]
    let postFillColor = defaultColor
    if (typeof key === 'string') {
      if (!PostFillColorMap[key]) {
        PostFillColorMap[key] = colorList[currentColorListIndex]
        currentColorListIndex = (currentColorListIndex + 1) % colorList.length
      }
      postFillColor = PostFillColorMap[key]
    }
    return {
      ...entry,
      [colorField]: postFillColor
    }
  })
}

export const withCustomizableTooltip = <Data extends Record<string, any> = {}, Props extends Record<string, any> = {}>(
  tooltipLabel: { default: GraphTextBuilder<Data, Props>; customize?: GraphTextBuilder<Data, Props> },
  tooltipValue: { default: GraphTextBuilder<Data, Props>; customize?: GraphTextBuilder<Data, Props> },
  customizeProps: Props
) => {
  const combinedTooltipLabelMap = mergeWithDefault(tooltipLabel.default, tooltipLabel.customize)
  const combinedTooltipValueMap = mergeWithDefault(tooltipValue.default, tooltipValue.customize)

  return (fieldNameList: (keyof Data)[]) => {
    const serializedTooltipFieldList = fieldNameList.map((fieldName) => {
      const labelBuilder = combinedTooltipLabelMap[fieldName]
      const valueBuilder = combinedTooltipValueMap[fieldName]
      const label = typeof labelBuilder === 'string' ? labelBuilder : labelBuilder(customizeProps)
      const value = typeof valueBuilder === 'string' ? valueBuilder : valueBuilder(customizeProps)

      return `"${label}": ${value}`
    })

    return `{ ${serializedTooltipFieldList.join(',')} }`
  }
}

export const populate = (key: string | string[], value: any | any[]) => {
  const keyList = Array.isArray(key) ? key : [key]
  const valueList = Array.isArray(value) ? value : [value]
  const obj = keyList.reduce<Record<string, any>>((prev, curr, index) => {
    const currValue = valueList[index]
    if (currValue !== null && currValue !== undefined) prev[curr] = valueList[index]

    return prev
  }, {})

  return obj
}

export const defaultAxis: (font?: string, fontSize?: number) => Partial<Axis> = (font = defaultFont) => ({
  titleFontSize: 12, //xAxisName font size
  titleFontWeight: 500,
  grid: true,
  gridColor: '#efefef',
  offset: 5,
  titlePadding: 15,
  labelFont: font,
  labelFontSize: 10,
  labelSeparation: 5,
  titleFont: font
})

export const defaultLegendSchema: (props: {
  fill?: string
  font?: string
  legend?: string
  customized?: boolean
}) => Partial<Legend> = ({ fill = 'bgColor', font = defaultFont, legend = 'Legend', customized = false }) => ({
  fill,
  orient: 'bottom',
  symbolType: 'square',
  symbolSize: 100,
  titleFontSize: { value: 12 }, //legend font size
  labelFontSize: { value: 12 },
  direction: 'horizontal',
  offset: 20,
  title: legend,
  titleOrient: 'left',
  labelFont: font,
  titleFont: font,
  encode: {
    symbols: {
      name: 'legendSymbol',
      interactive: true,
      enter: {
        fillOpacity: { value: 1 }
      }
    },
    labels: {
      name: 'legendLabel',
      interactive: true,
      update: {
        text: {
          signal: customized ? 'scale("legendLabelScale", datum["value"])' : 'datum["value"]'
        },
        tooltip: { signal: customized ? 'scale("legendLabelScale", datum["value"])' : 'datum["value"]' }
      }
    }
  }
})

export const clamp = (min: number, cur: number, max: number) => {
  return cur <= min ? min : cur >= max ? max : cur
}

export const GRAPH_INNER_TEXT_COLOR = '#787885'
export const GRAPH_OUTER_TEXT_COLOR = '#232334'
export const GRAPH_OUTER_TEXT_SUB_COLOR = '#343456'
export const GRAPH_LABEL_LIMIT = 200
