interface IProps {
  styles: any
  num: number
}

const LoadingItems = ({ num, styles }: IProps) => {
  return (
    <>
      {Array.from({ length: num }, () => (
        <div className='frame-items-load' style={{ ...styles }}></div>
      ))}
    </>
  )
}

export default LoadingItems
