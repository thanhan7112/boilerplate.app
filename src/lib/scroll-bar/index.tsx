import { FC, useEffect, useRef, useState } from 'react';
import ScrollBar, { ScrollBarProps } from 'react-perfect-scrollbar';
import elementResizeEvent from "element-resize-event";
interface FCScrollBar extends ScrollBarProps {
	height: number | string;
	disabled?: boolean;
	contentClassName?: string;
}

const CommonScrollBar: FC<FCScrollBar> = ({ disabled, children, options, height, style, onScroll, contentClassName, ...props }) => {
	const cRef = useRef<HTMLDivElement>(null)
	const [scrollRef, setScrollRef] = useState<any | null>(null);
	const [containerRef, setContainerRef] = useState<HTMLElement>();
	const [position, setPosition] = useState<{ top: number; left: number }>({ top: 0, left: 0 });
	useEffect(() => {
		scrollRef && scrollRef.updateScroll();
		cRef && cRef.current && elementResizeEvent(cRef.current, () => {
			scrollRef && scrollRef.updateScroll();
		})
	}, [cRef, scrollRef]);

	useEffect(() => {
		if (disabled) {
			setPosition({
				top: containerRef?.scrollTop || 0,
				left: containerRef?.scrollLeft || 0
			})
		}
	}, [disabled])

	return <ScrollBar
		style={{ height: height, width: "100%", ...style }}
		ref={ref => setScrollRef(ref)}
		containerRef={containerRef => setContainerRef(containerRef)}
		{...props}
		options={{
			...options,
			wheelPropagation: false,
		}}
		onScroll={(e) => {
			if (disabled && containerRef) {
				containerRef.scrollTop = position.top;
				containerRef.scrollLeft = position.left;
			}
			onScroll && onScroll(e);
		}}
	>
		<div ref={cRef} className={contentClassName}>
			{children}
		</div>
	</ScrollBar >
}
export default CommonScrollBar;