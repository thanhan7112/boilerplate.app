import React from 'react';
import styled from 'styled-components';
import { Button, Collapse } from 'antd';

export type UncacheLink = React.AnchorHTMLAttributes<HTMLAnchorElement>;
export const UncacheLink = ({
    href,
    children,
    ...rest
}: UncacheLink) => {
    return <a href={href} {...rest}>{children}</a>;
};

// [PAGE]
export const UndesiredPageContainer = styled.div`
    /* width: 100%; */
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    color: var(--color-normal);
    padding: var(--spacing-6xl);
    row-gap: var(--spacing);
    background-color: #fafafc;

    .error-status-code {
        vertical-align: baseline;
        font-size: 60px;
        font-weight: bold;
        line-height: 1;
    }

    .error-name {
        flex: 0;
        text-align: center;
        .anticon {
            margin-right: var(--spacing-30);
            vertical-align: baseline;
            font-size: 46px;
        }
        .message {
            font-size: 46px;
            line-height: 56px;
            font-weight: bold;
            text-transform: uppercase;
            display: inline-block;
            vertical-align: bottom;
        }
    }
`;

// [CONTENT]
const UndesiredContentContainer = styled.div`
    margin-top: var(--spacing);
    font-size: var(--fs-8);
    text-align: center;
    /* flex: 1; */
    .error-description {
        margin-bottom: var(--spacing-xl);
    }
    .error-sub-description {
        margin-top: var(--spacing);
        font-size: var(--fs-4);
    }
    .back-button {
        min-width: 12rem;
        height: unset;
        font-size: var(--fs-2xl);
        border-radius: var(--br-max);
    }
`;

export type UndesiredContent = {
    description: React.ReactNode,
    subDescription?: React.ReactNode,
    onBack?: () => void,
    onBackLabel?: React.ReactNode,
}
export const UndesiredContent = ({
    description,
    subDescription,
    onBack = () => {
        window.history.back();
    },
    onBackLabel = 'Back',
}: UndesiredContent) => {
    return <UndesiredContentContainer className="error-content">
        <div className="error-description">
            {description}
            <div className="error-sub-description">{subDescription}</div>
        </div>
        {onBackLabel && <Button type="primary" className="back-button" onClick={onBack}>{onBackLabel}</Button>}
    </UndesiredContentContainer>;
};

// [FOOTER]
const UndesiredFooterContainer = styled.div`
    flex: 1;
    font-size: var(--fs-3xl);
    color: var(--color-faint);
    display: flex;
    flex-direction: column;
    justify-content: flex-end;

    .output {
        flex: 1;
    }

    .error-output-collapse {
        border: 0;
        .error-output-collapse-panel {
            border-bottom: 0;
        }
        .ant-collapse-header,
        .show-error-btn {
            font-size: var(--fs-3xl);
        }
        .show-error-btn, 
        .ant-collapse-content-box {
            padding: 0;
        }
        .ant-collapse-content-box span,
        .ant-collapse-content {
            border: 0;
            
        }
    }
    
    .link-section {
        display: flex;
        flex-direction: column;
    }

    p {
        margin-bottom: var(--spacing);
        text-align: center;
    }
    .list-link {
        text-align: center;
        color: var(--color-extraFaint);
        margin-bottom: var(--spacing);
    }
    .contact-link {
        text-align: center;
    }

    a {
        color: #1890ff;
        cursor: pointer;
        text-decoration: none;
        background-color: transparent;
        outline: none;
        transition: color .3s;
    }
`;

const UndesiredOutput = styled.span`
    background-color: var(--main-tertiaryLighter);
    border-radius: var(--br);
    border: var(--bd);
    display: inline-block;
    line-height: var(--lh-lg);
    padding: var(--spacing-xs);
    white-space: pre-wrap;
    font-family: monospace;
    font-size: var(--fs-lg);
    min-width: 250px;
    max-width: 50vw;
    word-break: break-word;
`;
export type UndesiredNavigator = {
    output?: string,
    showOutput?: boolean,
    showButton?: boolean,
}
// eslint-disable-next-line @typescript-eslint/no-redeclare
export const UndesiredNavigator = ({
    output,
    showOutput = true,
    showButton = true,
}: UndesiredNavigator) => {
    // const [outputVisible, setOuputVisible] = React.useState<'' | '1'>('');

    return <UndesiredFooterContainer className="error-footer">
        {showOutput && <p className="output">
            <Collapse className='error-output-collapse'>
                <Collapse.Panel
                    className='error-output-collapse-panel'
                    showArrow={false}
                    header={
                        <span>
                            <Button type='link' className='show-error-btn'>
                                Click here
                            </Button>&nbsp;to view the error details:
                        </span>
                    }
                    key="1"
                >
                    <UndesiredOutput>
                        {output}
                    </UndesiredOutput>
                </Collapse.Panel>

            </Collapse>
        </p>}
        {showButton && <>
            <div className="link-section">
                <p className="instruction">Here are some helpful links instead:</p>
                <div className="list-link">
                    <UncacheLink href={'/' ?? ''}>Landing Page</UncacheLink>
                    {' | '}
                    <UncacheLink href={'/'} >Application Homepage</UncacheLink>
                </div>
                <div className="contact-link">
                    Technical Contact:&nbsp;
                    <a href='/'>thanhan7112@gmail.com</a>
                </div>
            </div>
        </>}
    </UndesiredFooterContainer>;
};