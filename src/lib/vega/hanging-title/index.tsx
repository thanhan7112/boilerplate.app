import React from 'react'
import styled from 'styled-components'
import { mergeClass } from '~/utils'

const HangingTitleContainer = styled.div`
  display: flex;
  position: absolute;
  top: 0;
  left: 0;
  background-color: #ffffffdd;
  z-index: 1000;
  border-bottom-right-radius: var(--br);
  user-select: auto;
  line-height: var(--lh-2xl);
  max-width: 100%;
  .hanging-title-value {
    font-size: var(--fs-md);
    font-weight: bold;
  }
  .hanging-title-tool {
    align-self: center;
    padding-left: var(--spacing);
    &:empty {
      padding-left: 0;
    }
  }
`

export type HangingTitle = {
  title?: React.ReactNode
  tool?: React.ReactNode
  className?: string
}
export const HangingTitle = ({ title, tool, className }: HangingTitle) => {
  return (
    <HangingTitleContainer className={mergeClass('hanging-title truncate', className)}>
      <div className='hanging-title-value truncate'>{title}</div>
      <div className='hanging-title-tool'>{tool}</div>
    </HangingTitleContainer>
  )
}
