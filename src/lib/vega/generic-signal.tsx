export const SIGNAL_NAME_FONT_SIZE = 'signalVar_fontSize';
export const getGenericFontSizeSignal = (formula: string) => {
    const minSize = 10;
    const maxSize = 16;
    return {
        name: SIGNAL_NAME_FONT_SIZE,
        value: maxSize,
        update: `clamp(${formula}, ${minSize}, ${maxSize})`,
    };
};