import React from 'react'
import { BarLineChartSchema } from './schema'
import BaseGraph from '../base-graph'
import { SpeclessBaseGraph } from '../../model-graph'
import { BarLineChartData, BarLineChartSpecificProps } from './model'

export type BaseBarLineGraph = SpeclessBaseGraph<BarLineChartData[], BarLineChartSpecificProps>
export const BarLineGraph = (props: BaseBarLineGraph) => {
  return <BaseGraph<BarLineChartData[], BarLineChartSpecificProps> {...props} spec={BarLineChartSchema} />
}
