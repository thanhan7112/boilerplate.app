/* eslint-disable @typescript-eslint/no-explicit-any */
import { Axis, SortOrder } from 'vega'
import { CurveInterpolateType, GraphTextBuilder } from '~/lib/vega/model-graph'
import { DefaultGraphColorSchema } from '../../base-graph'

export type BarLineChartData = {
  type: string
  secondaryType?: string
  amount: number
  secondaryAmount: number
  period: string
}

export type BarLineChartSpecificProps = {
  tooltipLabel?: GraphTextBuilder<BarLineChartData>
  tooltipValue?: GraphTextBuilder<BarLineChartData>
  periodAsDate?: boolean
  truncate?: boolean
  interpolation?: CurveInterpolateType
  axes?: {
    xAxis?: Partial<Axis>
    yAxis?: Partial<Axis>
    y2Axis?: Partial<Axis>
  }
  graphSort?: [SortOrder, SortOrder, SortOrder]
  /** @todo Support multiple line? */
  colorScheme?: {
    bar: keyof typeof DefaultGraphColorSchema
    line: keyof typeof DefaultGraphColorSchema
  }
  dataScaleMap?: {
    typeColor?: string
    legendLabel?: string
    secondaryLegendLabel?: string
    order?: string
  }
  signalListener?: {
    barClick?: (name: string, data: BarLineChartData) => void
    lineClick?: (name: string, data: BarLineChartData) => void
    legendClick?: (name: string, data: any) => void
  }
  lang?: {
    legend?: string
    xAxisName?: string
    yAxisName?: string
    y2AxisName?: string
  }
}
