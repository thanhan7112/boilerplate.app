import {
  defaultAxis,
  defaultFont,
  defaultLegendSchema,
  populate,
  withCustomizableTooltip,
  withPostFillColor
} from '~/utils'
import { DefaultGraphColorSchema } from '../base-graph'
import { SpecGenerator } from '../../model-graph'
import { LineChartData, LineChartSpecificProps } from './model'

export const LineChartSchema: SpecGenerator<LineChartData[], LineChartSpecificProps> = ({
  data,
  title,
  graphSort = ['ascending', 'ascending'],
  interpolation = 'monotone',
  tooltipLabel,
  tooltipValue,
  lang,
  colorScheme = DefaultGraphColorSchema.default.name,
  axes,
  font = defaultFont,
  dataScaleMap,
  truncate = true,
  periodAsDate = true
}) => {
  const { xAxis, yAxis } = axes ?? {}
  const { legend, xAxisName, yAxisName } = lang ?? {}
  const { legendLabel: legendLabelField, typeColor: nextTypeColorField, order: orderField } = dataScaleMap ?? {}
  const typeColorField = nextTypeColorField
  const isCustomizeLegendLabel = typeof legendLabelField === 'string'
  const postFilledData = withPostFillColor(data, 'type', typeColorField)
  const createTooltip = withCustomizableTooltip(
    {
      default: {
        amount: 'Amount',
        period: 'On', //Giai đoạn
        type: 'Type'
      },
      customize: tooltipLabel
    },
    {
      default: {
        amount: 'format(datum["amount"], ",")',
        period: ({ periodAsDate }) => (periodAsDate ? 'timeFormat(datum["period"], "%b %d, %Y")' : 'datum["period"]'),
        type: ({ isCustomizeLegendLabel }) =>
          isCustomizeLegendLabel ? 'scale("legendLabelScale", datum["type"])' : 'datum["type"]'
      },
      customize: tooltipValue
    },
    {
      periodAsDate,
      isCustomizeLegendLabel
    }
  )

  return {
    ...populate('title', title),
    autosize: 'fit',
    data: [
      {
        name: 'data_raw',
        values: postFilledData,
        format: { parse: { period: periodAsDate ? 'date' : 'string' } },
        transform: [
          {
            type: 'collect',
            sort: {
              field: [orderField ?? 'period', 'type'],
              order: graphSort
            }
          }
        ]
      }
    ],
    signals: [
      {
        name: 'interpolateTypeSignal',
        value: `${interpolation}`
      },
      {
        name: 'signalVar_currentHovered',
        value: {},
        on: [
          { events: 'symbol:mouseover', update: 'datum' },
          { events: 'symbol:mouseout', update: '{}' }
        ]
      },
      {
        name: 'pointClick',
        value: null,
        on: [
          {
            events: '@pointMark:click',
            update: '{ value: datum }',
            force: true
          }
        ]
      },
      {
        name: 'legendClick',
        value: null,
        on: [
          {
            events: '@legendSymbol:click, @legendLabel:click',
            update: '{ value: datum }',
            force: true
          }
        ]
      }
    ],
    scales: [
      {
        name: 'scalePeriod',
        type: 'point',
        range: 'width',
        domain: { data: 'data_raw', field: 'period' }
      },
      {
        name: 'scaleAmount',
        type: 'linear',
        range: 'height',
        nice: true,
        zero: true,
        domain: { data: 'data_raw', field: 'amount' }
      },
      {
        name: 'bgColor',
        type: 'ordinal',
        range: typeColorField ? { data: 'data_raw', field: typeColorField } : { scheme: colorScheme },
        domain: { data: 'data_raw', field: 'type' }
      },
      {
        name: 'legendLabelScale',
        type: 'ordinal',
        range: legendLabelField ? { data: 'data_raw', field: legendLabelField } : { data: 'data_raw', field: 'type' },
        domain: { data: 'data_raw', field: 'type' }
      },
      {
        name: 'txtColor',
        type: 'ordinal',
        range: ['#fff'],
        domain: {
          data: 'data_raw',
          field: 'type'
        }
      }
    ],
    axes: [
      {
        ...defaultAxis(font, 10),
        orient: 'left',
        scale: 'scaleAmount',
        maxExtent: 55,
        minExtent: 55,
        tickMinStep: 1,
        ...yAxis,
        ...populate('title', yAxisName)
      },
      {
        ...defaultAxis(font, 10),
        ...(periodAsDate
          ? {
              format: '%b %d %Y',
              formatType: 'time'
            }
          : {}),
        labelAlign: 'center',
        labelFontSize: 10,
        labelPadding: 15,
        labelLimit: truncate ? { signal: 'width / (domain("scalePeriod").length + 0.01) * 0.9' } : 200,
        labelOverlap: truncate ? false : 'parity',
        orient: 'bottom',
        scale: 'scalePeriod',
        ...xAxis,
        ...populate('title', xAxisName)
      }
    ],
    marks: [
      {
        type: 'group',
        name: 'valueLine',
        from: {
          facet: {
            name: 'series',
            data: 'data_raw',
            groupby: 'type'
          }
        },
        marks: [
          {
            type: 'line',
            name: 'lineMark',
            from: { data: 'series' },
            encode: {
              enter: {
                x: { scale: 'scalePeriod', field: 'period' },
                y: { scale: 'scaleAmount', field: 'amount' },
                stroke: { scale: 'bgColor', field: 'type' },
                strokeWidth: { value: 2 }
              },
              update: {
                interpolate: { signal: 'interpolateTypeSignal' },
                strokeOpacity: { value: 0.9 }
              }
            }
          }
        ]
      },
      {
        type: 'group',
        name: 'valuePoint',
        from: {
          facet: {
            name: 'pointData',
            data: 'data_raw',
            groupby: 'type'
          }
        },
        marks: [
          {
            type: 'symbol',
            name: 'pointMark',
            from: { data: 'pointData' },
            encode: {
              enter: {
                fill: {
                  scale: 'bgColor',
                  field: 'type'
                },
                tooltip: {
                  signal: createTooltip(['type', 'amount', 'period'])
                }
              },
              update: {
                shape: { value: 'circle' },
                x: { scale: 'scalePeriod', field: 'period' },
                y: { scale: 'scaleAmount', field: 'amount' },
                fillOpacity: {
                  signal:
                    '(datum.period === signalVar_currentHovered.period && datum.type === signalVar_currentHovered.type) ? 0.75 : 1'
                },
                size: {
                  signal:
                    '(datum.period === signalVar_currentHovered.period && datum.type === signalVar_currentHovered.type) ? 81 : 49'
                }
              }
            }
          },
          {
            type: 'rule',
            from: { data: 'pointData' },
            name: 'pointHorRule',
            encode: {
              enter: {
                stroke: { scale: 'bgColor', field: 'type' },
                strokeWidth: { value: 1 },
                strokeDash: { value: [6, 4] }
              },
              update: {
                x: { signal: 'range("scalePeriod")[1]' },
                x2: { value: 0 },
                y: { scale: 'scaleAmount', field: 'amount' },
                y2: { scale: 'scaleAmount', field: 'amount' },
                strokeOpacity: {
                  signal:
                    '(datum.period === signalVar_currentHovered.period && datum.type === signalVar_currentHovered.type) ? 0.75 : 0'
                }
              }
            }
          },
          {
            type: 'rule',
            from: { data: 'pointData' },
            name: 'pointVerRule',
            encode: {
              enter: {
                stroke: { scale: 'bgColor', field: 'type' },
                strokeWidth: { value: 1 },
                strokeDash: { value: [6, 4] }
              },
              update: {
                x: { scale: 'scalePeriod', field: 'period' },
                x2: { scale: 'scalePeriod', field: 'period' },
                y: { signal: 'range("scaleAmount")[0]' },
                y2: { signal: 'range("scaleAmount")[1] + 1' },
                strokeOpacity: {
                  signal:
                    '(datum.period === signalVar_currentHovered.period && datum.type === signalVar_currentHovered.type) ? 0.75 : 0'
                }
              }
            }
          }
        ]
      }
    ],
    legends: [defaultLegendSchema({ font, legend, customized: isCustomizeLegendLabel })]
  }
}
