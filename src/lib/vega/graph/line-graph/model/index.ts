/* eslint-disable @typescript-eslint/no-explicit-any */
import { Axis, SortOrder } from 'vega'
import { CurveInterpolateType, GraphTextBuilder, SpeclessBaseGraph } from '~/lib/vega/model-graph'

export type LineChartData = {
  amount: number
  period: string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
} & Record<string, any>

export type LineChartSpecificProps = {
  truncate?: boolean
  periodAsDate?: boolean
  interpolation?: CurveInterpolateType
  tooltipLabel?: GraphTextBuilder<LineChartData>
  tooltipValue?: GraphTextBuilder<LineChartData>
  graphSort?: [SortOrder, SortOrder]
  colorScheme?: any
  dataScaleMap?: {
    typeColor?: string
    legendLabel?: string
    order?: string
  }
  signalListener?: {
    pointClick?: (name: string, data: LineChartData) => void
    legendClick?: (name: string, data: LineChartData) => void
  }
  axes?: {
    xAxis?: Partial<Axis>
    yAxis?: Partial<Axis>
  }
  lang?: {
    legend?: string
    xAxisName?: string
    yAxisName?: string
  }
}

export type BaseLineGraph = SpeclessBaseGraph<LineChartData[], LineChartSpecificProps>
