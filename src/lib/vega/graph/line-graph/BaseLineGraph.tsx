import React from 'react'
import BaseGraph from '../base-graph'
import { LineChartSchema } from './schema'
import { BaseLineGraph, LineChartData, LineChartSpecificProps } from './model'

export const LineGraph = (props: BaseLineGraph) => {
  return <BaseGraph<LineChartData[], LineChartSpecificProps> {...props} spec={LineChartSchema} />
}
