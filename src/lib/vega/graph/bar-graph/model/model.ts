export type BarChartData = {
  type: string
  amount: number
  period: string
} & Record<string, any>
