import React from 'react'
import { SpeclessBaseGraph } from '../../model-graph'
import { BarChartData } from './model/model'
import { BarChartSchema, BarChartSpecificProps } from './default'
import BaseGraph from '../base-graph'

export type BaseBarGraph = SpeclessBaseGraph<BarChartData[], BarChartSpecificProps>
export const BaseBarGraph = (props: BaseBarGraph) => {
  return <BaseGraph<BarChartData[], BarChartSpecificProps> {...props} spec={BarChartSchema} />
}
