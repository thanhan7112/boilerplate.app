import { Axis, SortOrder } from 'vega'
import { GraphTextBuilder, SpecGenerator } from '../../model-graph'
import { BarChartData } from './model/model'
import { DefaultGraphColorSchema } from '../base-graph'
import {
  GRAPH_INNER_TEXT_COLOR,
  GRAPH_LABEL_LIMIT,
  defaultAxis,
  defaultFont,
  defaultLegendSchema,
  populate,
  withCustomizableTooltip,
  withPostFillColor
} from '~/utils'
import { SIGNAL_NAME_FONT_SIZE, getGenericFontSizeSignal } from '../../generic-signal'

export type BarChartSpecificProps = {
  tooltipLabel?: GraphTextBuilder<BarChartData>
  tooltipValue?: GraphTextBuilder<BarChartData>
  periodAsDate?: boolean
  truncate?: boolean
  axes?: {
    xAxis?: Partial<Axis>
    yAxis?: Partial<Axis>
  }
  graphSort?: [SortOrder, SortOrder]
  colorScheme?: keyof typeof DefaultGraphColorSchema
  dataScaleMap?: {
    typeColor?: string
    legendLabel?: string
    order?: string
  }
  signalListener?: {
    barClick?: (name: string, data: BarChartData) => void
    legendClick?: (name: string, data: BarChartData) => void
  }
  lang?: {
    legend?: string
    xAxisName?: string
    yAxisName?: string
  }
}
export const BarChartSchema: SpecGenerator<BarChartData[], BarChartSpecificProps> = ({
  data,
  title,
  lang,
  graphSort = ['ascending', 'ascending'],
  tooltipLabel,
  tooltipValue,
  colorScheme = DefaultGraphColorSchema.default.name,
  axes,
  font = defaultFont,
  truncate = true,
  periodAsDate = true,
  dataScaleMap
}) => {
  const { xAxis, yAxis } = axes ?? {}
  const { legend, xAxisName, yAxisName } = lang ?? {}

  const { legendLabel: legendLabelField, typeColor: nextTypeColorField, order: orderField } = dataScaleMap ?? {}
  const typeColorField = nextTypeColorField
  const isCustomizeLegendLabel = typeof legendLabelField === 'string'
  const postFilledData = withPostFillColor(data, 'type', typeColorField)
  const createTooltip = withCustomizableTooltip(
    {
      default: {
        amount: 'Amount',
        period: 'On',
        type: 'Type'
      },
      customize: tooltipLabel
    },
    {
      default: {
        amount: 'format(datum["amount"], ",")',
        period: ({ periodAsDate }) => (periodAsDate ? 'timeFormat(datum["period"], "%b %d, %Y")' : 'datum["period"]'),
        type: ({ isCustomizeLegendLabel }) =>
          isCustomizeLegendLabel ? 'scale("legendLabelScale", datum["type"])' : 'datum["type"]'
      },
      customize: tooltipValue
    },
    {
      periodAsDate,
      isCustomizeLegendLabel
    }
  )

  return {
    ...populate('title', title),
    autosize: 'fit',
    data: [
      {
        name: 'data_raw',
        values: postFilledData,
        format: { parse: { period: periodAsDate ? 'date' : 'string' } },
        transform: [
          {
            type: 'collect',
            sort: {
              field: [orderField ?? 'period', 'type'],
              order: graphSort
            }
          }
        ]
      }
    ],
    signals: [
      {
        name: 'currentHovered',
        value: {},
        on: [
          { events: 'rect:mouseover', update: 'datum' },
          { events: 'rect:mouseout', update: '{}' }
        ]
      },
      /**
       * Tham số lệ thuộc: Độ dài tổng => Chia cho mỗi vùng => Chia cho số bar trong một vùng => Chia cho số ký tự dài nhất có thể xuất hiện trong một bar
       *
       * Font size cho số liệu trên đầu của mỗi bar.
       * * 0.01 là tham số phụ để ngăn xảy ra trường hợp chia cho 0.
       * * 0.6 là tỷ lệ tương đối giữa độ dài chữ và font size.
       * * 0.85 là tỷ lệ tương đối giữa chữ và độ dài của bar (ta muốn chữ hơi nhỏ một chút chứ không dính sát vào bar)
       */
      getGenericFontSizeSignal(`width
                / (domain("scalePeriod").length + 0.01)
                / (domain("bgColor").length + 0.01)
                / (toString(domain("scaleAmount")[1]).length + 0.01)
                / 0.6 * 0.85`),
      {
        name: 'barClick',
        value: null,
        on: [
          {
            events: '@barMark:click',
            update: '{ value: datum }',
            force: true
          }
        ]
      },
      {
        name: 'legendClick',
        value: null,
        on: [
          {
            events: '@legendSymbol:click, @legendLabel:click',
            update: '{ value: datum }',
            force: true
          }
        ]
      }
    ],
    scales: [
      {
        name: 'scalePeriod',
        type: 'band',
        range: 'width',
        domain: { data: 'data_raw', field: 'period' },
        padding: 0.2
      },
      {
        name: 'scaleAmount',
        type: 'linear',
        range: 'height',
        nice: true,
        zero: true,
        domain: { data: 'data_raw', field: 'amount' }
      },
      {
        name: 'bgColor',
        type: 'ordinal',
        range: typeColorField ? { data: 'data_raw', field: typeColorField } : { scheme: colorScheme },
        domain: { data: 'data_raw', field: 'type' }
      },
      {
        name: 'legendLabelScale',
        type: 'ordinal',
        range: legendLabelField ? { data: 'data_raw', field: legendLabelField } : { data: 'data_raw', field: 'type' },
        domain: { data: 'data_raw', field: 'type' }
      },
      {
        name: 'scaleRuler',
        type: 'point',
        range: 'width',
        domain: { data: 'data_raw', field: 'period' }
      }
    ],
    axes: [
      {
        ...defaultAxis(font, 10),
        orient: 'left',
        scale: 'scaleAmount',
        maxExtent: 55,
        minExtent: 55,
        tickMinStep: 1,
        ...yAxis,
        ...populate('title', yAxisName)
      },
      {
        ...defaultAxis(font, 10),
        ...(periodAsDate
          ? {
              format: '%b %d %Y',
              formatType: 'time'
            }
          : {}),
        grid: false,
        labelAlign: 'center',
        labelFontSize: 10,
        labelPadding: 15,
        labelLimit: truncate ? { signal: 'width / (domain("scalePeriod").length + 0.01) * 0.9' } : GRAPH_LABEL_LIMIT,
        labelOverlap: truncate ? false : 'parity',
        orient: 'bottom',
        scale: 'scalePeriod',
        ...xAxis,
        ...populate('title', xAxisName)
      }
    ],
    marks: [
      {
        type: 'group',
        name: 'valueBar',
        from: {
          facet: {
            name: 'barData',
            data: 'data_raw',
            groupby: 'period'
          }
        },
        encode: {
          enter: {
            x: { scale: 'scalePeriod', field: 'period' }
          }
        },
        signals: [{ name: 'width', update: 'bandwidth("scalePeriod")' }],
        scales: [
          {
            name: 'barGroupScale',
            type: 'band',
            range: 'width',
            domain: { data: 'barData', field: 'type' }
          }
        ],
        marks: [
          {
            name: 'barMark',
            from: { data: 'barData' },
            type: 'rect',
            encode: {
              enter: {
                x: { scale: 'barGroupScale', field: 'type' },
                width: { scale: 'barGroupScale', band: 1 },
                y: { scale: 'scaleAmount', field: 'amount' },
                y2: { scale: 'scaleAmount', value: 0 },
                fill: { scale: 'bgColor', field: 'type' },
                tooltip: {
                  signal: createTooltip(['type', 'amount', 'period'])
                }
              },
              update: {
                fillOpacity: {
                  signal: '(datum.period === currentHovered.period && datum.type === currentHovered.type) ? 0.75 : 1'
                }
              }
            }
          },
          {
            type: 'text',
            name: 'topText',
            from: { data: 'barData' },
            encode: {
              enter: {
                fill: { value: GRAPH_INNER_TEXT_COLOR },
                font: { value: font },
                fontSize: { signal: SIGNAL_NAME_FONT_SIZE },
                fontWeight: { value: 'bold' },
                fillOpacity: { value: 1 }
              },
              update: {
                x: {
                  scale: 'barGroupScale',
                  field: 'type',
                  offset: { signal: 'bandwidth("barGroupScale") / 2' }
                },
                y: { scale: 'scaleAmount', field: 'amount', offset: -5 },
                text: { field: 'amount' },
                align: { value: 'center' },
                /**
                 * Ta không show text nếu nó là 0
                 */
                fillOpacity: {
                  signal: 'datum["amount"] > 0 ? 1 : 0'
                }
              }
            }
          }
        ]
      },
      {
        type: 'rule',
        from: { data: 'data_raw' },
        name: 'pointHorRule',
        encode: {
          enter: {
            stroke: { scale: 'bgColor', field: 'type' },
            strokeWidth: { value: 1 },
            strokeDash: { value: [6, 2] },
            zindex: { value: 100 }
          },
          update: {
            x: { signal: 'range("scaleRuler")[0]' },
            x2: { signal: 'range("scaleRuler")[1]' },
            y: { scale: 'scaleAmount', field: 'amount' },
            y2: { scale: 'scaleAmount', field: 'amount' },
            strokeOpacity: {
              signal: '(datum.period === currentHovered.period && datum.type === currentHovered.type) ? 0.75 : 0'
            }
          }
        }
      }
    ],
    legends: [defaultLegendSchema({ font, legend, customized: isCustomizeLegendLabel })]
  }
}
