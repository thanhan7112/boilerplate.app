import React from 'react'
import { SpeclessBaseGraph } from '../../model-graph'
import { PieChartData, PieChartSpecificProps } from './model'
import { PieChartSchema } from './schema'
import BaseGraph from '../base-graph'

export type BasePieGraph = SpeclessBaseGraph<PieChartData[], PieChartSpecificProps>
export const BasePieGraph = (props: SpeclessBaseGraph<PieChartData[], PieChartSpecificProps>) => {
  return <BaseGraph<PieChartData[], PieChartSpecificProps> {...props} spec={PieChartSchema} />
}
