import { GraphTextBuilder } from '~/lib/vega/model-graph'
import { DefaultGraphColorSchema } from '../../base-graph'

export type PieChartData = {
  type: string
  amount: number
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any
}

export type PieChartSpecificProps = {
  legendOption?: {
    atBottom?: boolean
    columnWidth?: number
  }
  tooltipLabel?: GraphTextBuilder<PieChartData & { ratio: string }>
  tooltipValue?: GraphTextBuilder<PieChartData & { ratio: string }>
  colorScheme?: keyof typeof DefaultGraphColorSchema
  dataScaleMap?: {
    typeColor?: string
    legendLabel?: string
  }
  dataAggregateMap?: {
    pieRadius?: (data: PieChartData[]) => number
    total?: (data: PieChartData[]) => string | number | null
  }
  signalListener?: {
    pieClick?: (name: string, value: PieChartData) => void
    legendClick?: (name: string, value: PieChartData) => void
  }
  lang?: {
    legend?: string
    unit?: string
    subTitle?: string
    disable?: boolean
  }
}
