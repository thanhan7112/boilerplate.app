/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  GRAPH_OUTER_TEXT_COLOR,
  GRAPH_OUTER_TEXT_SUB_COLOR,
  defaultFont,
  defaultLegendSchema,
  populate,
  withCustomizableTooltip,
  withPostFillColor
} from '~/utils'
import { SpecGenerator } from '../../model-graph'
import { DefaultGraphColorSchema } from '../base-graph'
import { PieChartData, PieChartSpecificProps } from './model'

export const PieChartSchema: SpecGenerator<PieChartData[], PieChartSpecificProps> = ({
  data,
  title = '',
  size,
  colorScheme = DefaultGraphColorSchema.default.name,
  lang,
  font = defaultFont,
  dataScaleMap,
  dataAggregateMap,
  legendOption,
  tooltipLabel,
  tooltipValue
}) => {
  const { legend, unit = '', subTitle, disable } = lang ?? {}
  const { pieRadius, total } = dataAggregateMap ?? {}
  const { atBottom: legendAtBottom = false, columnWidth: legendColumnWidth = 150 } = legendOption ?? {}
  const { width, height } = size
  const selectedSize = width > height ? height : width
  const padding = 15
  const pieToText = 10
  const textHeight = 20
  const subtextHeight = 20
  /**
   * Radius của đồ thị: Lấy chiều ngắn hơn của đồ thị * 0.45
   */
  const radius = pieRadius
    ? pieRadius(data)
    : Math.floor((selectedSize - padding * 2 - pieToText - textHeight - (subTitle ? subtextHeight : 0)) * 0.45)
  const { typeColor: typeColorField, legendLabel: legendLabelField } = dataScaleMap ?? {}
  const isCustomizeLegendLabel = typeof legendLabelField === 'string'
  const postFilledData = withPostFillColor(data, 'type', typeColorField)
  const createTooltip = withCustomizableTooltip(
    {
      default: {
        amount: 'Amount', //Label
        ratio: 'Ratio', //Label
        type: 'Type' //Label
      },
      customize: tooltipLabel
    },
    {
      default: {
        amount: 'format(datum["agg_amount"], ",")',
        ratio: 'format(datum["percent_text"] , "2.2f") + "%"',
        type: ({ isCustomizeLegendLabel }) =>
          isCustomizeLegendLabel ? 'scale("legendLabelScale", datum["type"])' : 'datum["type"]'
      },
      customize: tooltipValue
    },
    {
      isCustomizeLegendLabel
    }
  )
  const totalAmount = total ? total(data) : 0

  return {
    ...populate('title', title),
    autosize: 'fit',
    data: [
      {
        name: 'data_raw',
        values: postFilledData,
        transform: [
          {
            type: 'collect',
            sort: {
              field: ['amount', 'type'],
              order: ['descending', 'ascending']
            }
          }
        ]
      },
      {
        name: 'data_sum',
        values: data,
        transform: [
          {
            type: 'aggregate',
            fields: ['amount'],
            ops: ['sum'],
            as: ['total']
          }
        ]
      },
      {
        name: 'data_pie',
        source: 'data_raw',
        transform: [
          {
            type: 'aggregate',
            groupby: ['type'],
            fields: ['amount', 'amount'],
            ops: ['sum', 'values'],
            as: ['agg_amount', 'data']
          },
          {
            type: 'pie',
            field: 'agg_amount',
            as: ['start_angle', 'end_angle']
          },
          {
            type: 'formula',
            as: 'percent_text',
            expr: '(datum["end_angle"] - datum["start_angle"])/(2 * PI)*100'
          }
        ]
      }
    ],
    signals: [
      {
        name: 'signalVar_legendColumn',
        value: 1,
        update: legendAtBottom
          ? `clamp(floor(width / ${legendColumnWidth}), 1, 6)`
          : `clamp(floor((width - ${radius} * 2 - 40) / ${legendColumnWidth}), 1, 6)`
      },
      {
        name: '_current_hoverred',
        value: {},
        on: [
          { events: '*:mouseover', update: 'datum' },
          { events: '*:mouseout', update: '{}' }
        ]
      },
      {
        name: 'pieClick',
        value: null,
        on: [
          {
            events: '@pieMark:click, @pieTextIn:click, @pieTextOut:click',
            update: '{ value: datum }',
            force: true
          }
        ]
      },
      {
        name: 'legendClick',
        value: null,
        on: [
          {
            events: '@legendSymbol:click, @legendLabel:click',
            update: '{ value: datum }',
            force: true
          }
        ]
      }
    ],
    scales: [
      {
        name: 'scaleBgColor',
        type: 'ordinal',
        range: typeColorField ? { data: 'data_raw', field: typeColorField } : { scheme: colorScheme },
        domain: { data: 'data_raw', field: 'type' }
      },
      {
        name: 'legendLabelScale',
        type: 'ordinal',
        range: legendLabelField ? { data: 'data_raw', field: legendLabelField } : { data: 'data_raw', field: 'type' },
        domain: { data: 'data_raw', field: 'type' }
      },
      {
        name: 'txtColor',
        type: 'ordinal',
        range: ['#fff'],
        domain: {
          data: 'data_raw',
          field: 'type'
        }
      }
    ],
    marks: [
      {
        name: 'pieMark',
        type: 'arc',
        interactive: true,
        from: { data: 'data_pie' },
        encode: {
          enter: {
            fill: {
              scale: 'scaleBgColor',
              field: 'type'
            },
            startAngle: { field: 'start_angle' },
            endAngle: { field: 'end_angle' },
            innerRadius: { value: 0 },
            padAngle: { value: 0 },
            tooltip: {
              signal: createTooltip(['type', 'amount', 'ratio'])
            },
            stroke: { value: '#fff' }
          },
          update: {
            x: { signal: legendAtBottom ? 'width / 2' : `${radius} + ${padding}` },
            y: { signal: `${radius}` },
            fillOpacity: {
              signal: 'datum.type === _current_hoverred.type ? 0.75 : 1'
            },
            outerRadius: {
              signal: `datum.type === _current_hoverred.type ? ${radius + 5} : ${radius}`
            }
          }
        }
      },
      {
        name: 'pieTextIn',
        type: 'text',
        from: { data: 'data_pie' },
        encode: {
          enter: {
            fill: { scale: 'txtColor', field: 'type' },
            font: { value: font },
            fontSize: { value: 14 },
            align: { value: 'center' },
            baseline: { value: 'middle' },
            tooltip: {
              signal: createTooltip(['type', 'amount', 'ratio'])
            }
          },
          update: {
            text: [
              {
                test: '(datum["end_angle"] - datum["start_angle"]) < 0.1 * PI',
                signal: ''
              },
              { signal: 'format(datum["percent_text"] , "2.1~f") + "%"' }
            ],
            radius: {
              signal: `${radius / 1.5}`
            },
            x: { signal: legendAtBottom ? 'width / 2' : `${radius} + ${padding}` },
            y: { signal: `${radius}` },
            theta: { signal: '(datum["start_angle"] + datum["end_angle"])/2' }
          }
        }
      },
      ...(totalAmount == null
        ? ([] as any[])
        : [
            {
              name: 'pieTextOut',
              type: 'text',
              from: { data: 'data_sum' },
              encode: {
                enter: {
                  fill: { value: GRAPH_OUTER_TEXT_COLOR },
                  font: { value: font },
                  fontSize: { value: 15 },
                  align: { value: 'center' },
                  baseline: { value: 'top' }
                },
                update: {
                  text: {
                    signal: `"Total: " + ${total ? total(data) : 'datum["total"]'} + " " + "${unit}"`
                  },
                  x: { signal: legendAtBottom ? 'width / 2' : `${radius} + ${padding}` },
                  y: { signal: `${radius * 2} + ${pieToText}` }
                }
              }
            }
          ]),
      ...(subTitle
        ? [
            {
              name: 'pieSubTextOut',
              type: 'text',
              from: { data: 'data_sum' },
              encode: {
                enter: {
                  fill: { value: GRAPH_OUTER_TEXT_SUB_COLOR },
                  font: { value: font },
                  fontSize: { value: 13 },
                  align: { value: 'center' },
                  baseline: { value: 'top' }
                },
                update: {
                  text: { value: subTitle },
                  x: { signal: legendAtBottom ? 'width / 2' : `${radius} + ${padding}` },
                  y: { signal: `${radius * 2} + ${textHeight} + ${pieToText}` }
                }
              }
            }
          ]
        : ([] as any[]))
    ],
    legends:
      disable === true
        ? []
        : [
            {
              ...defaultLegendSchema({ font, legend }),
              titleOrient: 'top',
              fill: 'scaleBgColor',
              orient: legendAtBottom ? 'bottom-left' : 'top-right',
              rowPadding: 5,
              columns: { signal: 'signalVar_legendColumn' },
              /**
               * * 20 là limit của legend icon + spacing
               * * 40 là spacing xung quanh của legend box
               * */
              labelLimit: {
                signal: legendAtBottom
                  ? '(width - 40) / signalVar_legendColumn - 20'
                  : `(width - 40 - ${radius} * 2) / signalVar_legendColumn - 20`
              },
              direction: 'vertical',
              encode: {
                symbols: {
                  name: 'legendSymbol',
                  interactive: true,
                  enter: {
                    fillOpacity: { value: 1 }
                  }
                },
                labels: {
                  name: 'legendLabel',
                  interactive: true,
                  enter: {
                    font: { value: font }
                  },
                  update: {
                    text: { signal: legendLabelField ? 'scale("legendLabelScale", datum["value"])' : 'datum["value"]' },
                    tooltip: {
                      signal: legendLabelField ? 'scale("legendLabelScale", datum["value"])' : 'datum["value"]'
                    }
                  }
                }
              }
            }
          ]
  }
}
