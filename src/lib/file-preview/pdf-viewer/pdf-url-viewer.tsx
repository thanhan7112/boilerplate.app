/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState, useCallback, useRef } from 'react'
import { PDFDocumentProxy, PDFPageProxy } from 'pdfjs-dist'
import { pdfjs } from './index'
import PdfViewer from './viewer'
import LoadingSpinner from '~/lib/load/lds-spinner'

interface PdfUrlViewerProps {
  url: string
  [x: string]: any
  scale: number
  callBackProps: (total: number) => void
  onCurrentPage: (current: number) => void
}

const PdfUrlViewer: React.FC<PdfUrlViewerProps> = (props) => {
  const pdfRef = useRef<PDFDocumentProxy | null>(null)
  const [itemCount, setItemCount] = useState<number>(0)
  const [heightResize, setHeightResize] = useState(600)
  const [ready, setReady] = useState(false)
  const { url, callBackProps, scale, gap, onCurrentPage, ...others } = props

  useEffect(() => {
    const loadingTask = pdfjs.getDocument(url)
    loadingTask.promise.then(
      (pdf: PDFDocumentProxy) => {
        pdfRef.current = pdf
        setItemCount(pdf.numPages)
        callBackProps(pdf.numPages ?? 1)
        // Fetch the first page
        const pageNumber = 1
        pdf.getPage(pageNumber).then(() => {})
      },
      (reason: any) => {
        // PDF loading error
        console.error(reason)
      }
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [url])

  useEffect(() => {
    ;(async () => {
      setReady(false)
      if (pdfRef.current && itemCount > 0) {
        const defaultHeight =
          Math.max((await pdfRef.current.getPage(1)).getViewport({ scale }).height + (gap || 0) - 220, 0) || 600
        setHeightResize(defaultHeight)
      } else {
        setHeightResize(600)
      }
      setReady(true)
    })()
  }, [itemCount, scale, gap])

  const handleGetPdfPage = useCallback(async (index: number): Promise<PDFPageProxy> => {
    if (pdfRef.current) {
      return pdfRef.current.getPage(index + 1)
    } else {
      // Handle case when pdfRef.current is null
      return Promise.reject('PDF document is not loaded')
    }
  }, [])

  return (
    <div>
      {ready ? (
        <PdfViewer
          itemCount={itemCount}
          onCurrentPage={onCurrentPage}
          heightResize={heightResize}
          getPdfPage={handleGetPdfPage}
          scale={scale}
          gap={gap}
          {...others}
        />
      ) : (
        <LoadingSpinner />
      )}
    </div>
  )
}

export default PdfUrlViewer
