import React, { useEffect, useRef } from 'react'
import { pdfjs } from './index'
import { PDFPageProxy } from 'pdfjs-dist'
import './style.scss'

interface PdfPageProps {
  page: PDFPageProxy | null
  scale: number
}

const PdfPage: React.FC<PdfPageProps> = ({ page, scale }) => {
  const canvasRef = useRef<HTMLCanvasElement>(null)
  const textLayerRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (!page || !canvasRef.current) {
      return
    }
    const viewport = page.getViewport({ scale })
    const canvas = canvasRef.current
    const context = canvas.getContext('2d')
    if (!context) {
      return // Handle null context
    }

    canvas.height = viewport.height
    canvas.width = viewport.width

    const renderContext = {
      canvasContext: context,
      viewport
    }

    const renderTask = page.render(renderContext)
    renderTask.promise.then(function () {})

    page.getTextContent().then((textContent) => {
      if (!textLayerRef.current) {
        return
      }
      pdfjs.renderTextLayer({
        textContentSource: textContent,
        container: textLayerRef.current,
        viewport,
        textDivs: []
      })
    })
  }, [page, scale])

  return (
    <div className='pdf-page'>
      <canvas ref={canvasRef} />
      <div ref={textLayerRef} className='pdf_page-text_layer'></div>
    </div>
  )
}

export default PdfPage
