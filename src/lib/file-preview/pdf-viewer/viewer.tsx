/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useCallback, useEffect, useRef, useState } from 'react'
import useResizeObserver from 'use-resize-observer'
import { VariableSizeList } from 'react-window'
import PdfPage from './pdf-page'

interface PdfViewerProps {
  width?: number | string
  height?: number | string
  itemCount: number
  getPdfPage: (index: number) => Promise<any>
  scale: any
  gap?: number
  windowRef?: any
  onCurrentPage: (current: number) => void
  heightResize: number
}

const PdfViewer: React.FC<PdfViewerProps> = ({
  width,
  height,
  itemCount,
  getPdfPage,
  scale,
  gap,
  windowRef,
  onCurrentPage,
  heightResize
}) => {
  const [pages, setPages] = useState<any[]>([])
  const listRef = useRef<any>(null)
  const [scrollOffset, setScrollOffset] = useState(0)

  const { ref, height: internalHeight = heightResize } = useResizeObserver()

  const fetchPage = useCallback(
    async (index: number) => {
      if (!pages[index]) {
        const page = await getPdfPage(index)
        setPages((prevPages) => {
          const nextPages = [...prevPages]
          nextPages[index] = page
          return nextPages
        })
        listRef.current?.resetAfterIndex(index)
      }
    },
    [getPdfPage, pages]
  )

  const handleItemSize = useCallback(
    (index: number) => {
      const page = pages[index]
      if (page) {
        const viewport = page.getViewport({ scale })
        return viewport.height + gap
      }
      return 50
    },
    [pages, scale, gap]
  )

  const handleListRef = useCallback(
    (element: VariableSizeList<any> | null) => {
      listRef.current = element
      if (windowRef) {
        windowRef.current = element
      }
    },
    [windowRef]
  )

  useEffect(() => {
    listRef.current.resetAfterIndex(0)
  }, [scale])

  const containerStyle: React.CSSProperties = {
    width,
    height,
    border: '1px solid #ccc',
    background: '#ddd'
  }

  useEffect(() => {
    if ((scrollOffset * 100) / heightResize > 98) {
      onCurrentPage(2)
    }
    if ((scrollOffset * 100) / heightResize < 5) {
      onCurrentPage(1)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [scrollOffset])

  return (
    <div ref={ref} style={containerStyle}>
      <VariableSizeList
        ref={handleListRef}
        width={'100%'}
        onScroll={(v) => {
          setScrollOffset(v.scrollOffset)
        }}
        height={internalHeight}
        itemCount={itemCount}
        itemSize={handleItemSize}
      >
        {({ index, style }) => {
          fetchPage(index)
          return (
            <div style={style} className='flex flex-al-center flex-jc-center'>
              <PdfPage page={pages[index]} scale={scale} />
            </div>
          )
        }}
      </VariableSizeList>
    </div>
  )
}

PdfViewer.defaultProps = {
  width: '100%',
  height: '100%',
  scale: 1,
  gap: 40
}

export default PdfViewer
