/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC, useRef, useState } from 'react'
import PdfUrlViewer from '../pdf-viewer/pdf-url-viewer'
import { Button, Modal, Pagination } from 'antd'
import './style.scss'
import { fileCategorize, getContentTypeIcon } from '~/utils/formatFile'

interface Iprops {
  open: boolean
  onCancel: () => void
}

const ModalPreview: FC<Iprops> = ({ open, onCancel }) => {
  const [scale, setScale] = useState(1)
  const [total, setTotal] = useState(1)
  const windowRef = useRef<any>()
  const [current, setCurrent] = useState(1)
  const url = 'http://192.168.7.21:8080/filemanagement/public/document/preview?key=760a8fee93021daff8c0037992530a55'
  const scrollToItem = (page: number) => {
    windowRef.current && windowRef.current.scrollToItem(page - 1, 'start')
  }

  return (
    <Modal
      className='custom-modal-preview'
      title='Preview'
      open={open}
      onCancel={onCancel}
      width={'96%'}
      footer={[
        <div className='flex flex-row flex-jc-between flex-al-center gap-sm'>
          <div className='flex flex-row gap-sm'>
            <Button onClick={() => setScale((v) => v + 0.1)}>+</Button>
            <Button onClick={() => setScale((v) => v - 0.1)}>-</Button>
          </div>
          <Pagination
            onChange={(e) => {
              setCurrent(e)
              scrollToItem(e)
            }}
            current={current}
            pageSize={1}
            defaultCurrent={1}
            total={total}
            showQuickJumper
          />
        </div>
      ]}
    >
      <div className='layout-preview-content'>
        <div className='list-files'>
          {[1, 2, 3].map(() => {
            const fileFormat = fileCategorize('pdf')
            const icon = getContentTypeIcon(fileFormat)
            return (
              <div
                className='frame-file'
                onClick={() => {
                  setCurrent(1)
                  scrollToItem(1)
                }}
              >
                {icon}
                <div className='flex'>
                  <div className='file text-flex-truncate'>pdfjs-dist.pdf</div>
                </div>
                <div className='flex flex-al-center flex-jc-center'>
                  <i className='fa fa-download' aria-hidden='true' onClick={() => console.log('eee')}></i>
                </div>
              </div>
            )
          })}
        </div>
        <div className='content'>
          <PdfUrlViewer
            url={url}
            scale={scale}
            onCurrentPage={setCurrent}
            windowRef={windowRef}
            callBackProps={setTotal}
          />
        </div>
      </div>
    </Modal>
  )
}

export default ModalPreview
