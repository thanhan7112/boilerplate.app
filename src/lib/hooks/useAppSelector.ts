import { TypedUseSelectorHook, useSelector } from 'react-redux'
import { TypeRootReducer } from '~/store/reducers/default'

const useAppSelector: TypedUseSelectorHook<TypeRootReducer> = useSelector
export default useAppSelector
