export interface UseLocalDataProps {
  menu: {
    getOpenKeys: (accountCurrentId?: number) => string[]
    setOpenKeys: (keys: string[], accountCurrentId?: number) => void

    getCollapsed: () => boolean
    setCollapsed: (collapsed: boolean) => void
    onToggleCollapsed: () => void

    getScrollTop: (accountCurrentId?: number) => number
    setScrollTop: (scrollTop: number, accountCurrentId?: number) => void
  }
}

const useLocalData = (): UseLocalDataProps => {
  const getLocalStorageData = () => {
    const localDataStr = localStorage.getItem('locale-data')
    try {
      const localDataObj = JSON.parse(localDataStr || '')
      return localDataObj || {}
    } catch (e) {}
  }

  const setLocalStorageData = (currentData: any) => {
    try {
      localStorage.setItem('locale-data', JSON.stringify(currentData))
    } catch (e) {
      console.log(e)
    }
  }

  const getOpenKeys = (): string[] => {
    const currentData = getLocalStorageData()
    if (currentData?.open_keys && Array.isArray(currentData.open_keys)) {
      return currentData.open_keys
    }
    return []
  }

  const setOpenKeys = (keys: string[]) => {
    const currentData = getLocalStorageData()
    setLocalStorageData({ ...currentData, open_keys: keys })
  }

  const getCollapsed = (): boolean => {
    const currentData = getLocalStorageData()
    return currentData?.collapsed === true
  }

  const setCollapsed = (collapsed: boolean) => {
    const currentData = getLocalStorageData()
    setLocalStorageData({ ...currentData, collapsed })
  }

  const onToggleCollapsed = () => {
    const collapsed = getCollapsed()
    setCollapsed(!collapsed)
  }

  const getScrollTop = (): number => {
    const currentData = getLocalStorageData()
    if (currentData?.sider_bar_scroll_top && typeof currentData.sider_bar_scroll_top === 'number') {
      return currentData.sider_bar_scroll_top
    }
    return 0
  }

  const setScrollTop = (scrollTop: number) => {
    const currentData = getLocalStorageData()
    setLocalStorageData({ ...currentData, sider_bar_scroll_top: scrollTop })
  }

  return {
    menu: {
      getOpenKeys: getOpenKeys,
      setOpenKeys: setOpenKeys,
      getCollapsed: getCollapsed,
      setCollapsed: setCollapsed,
      onToggleCollapsed: onToggleCollapsed,
      getScrollTop: getScrollTop,
      setScrollTop: setScrollTop
    }
  }
}

export default useLocalData
