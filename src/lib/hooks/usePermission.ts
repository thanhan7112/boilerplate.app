import { parse } from 'qs'
import { useSelector } from 'react-redux'
import { useLocation } from 'react-router-dom'
import { PermissionItemProps } from '~/model'
import { TypeRootReducer } from '~/store/reducers/default'
import {
  findPermissionByCode as pFindPermissionByCode,
  findPermissionById as pFindPermissionById,
  findPermissionByUrl as pFindPermissionByUrl
} from '~/utils'

export interface UsePermissionProps {
  activeKey: string | undefined
  findPermissionByCode: (permissionContext: string) => PermissionItemProps | undefined
  findPermissionById: (permissionId: number) => PermissionItemProps | undefined
  findPermissionByUrl: (url: string) => PermissionItemProps | undefined
  checkPermission: (permissionContext?: string, type?: 'ALL' | 'PAGE') => boolean
  getId: (permissionContext?: string, type?: 'ALL' | 'PAGE') => number | undefined
  getUrl: (permissionContext?: string, type?: 'ALL' | 'PAGE') => string | undefined
  getPagePermission: () => PermissionItemProps | undefined
  name?: string
  allowAccess: boolean
}
const usePermission = (): UsePermissionProps => {
  const location = useLocation()
  const permissions = useSelector((state: TypeRootReducer) => state.permission)
  const qs = parse(location.search, { ignoreQueryPrefix: true })
  const p = qs.p as string | undefined
  const pID = p && !isNaN(parseInt(p, 0)) ? parseInt(p, 0) : null
  const pagePermission = pFindPermissionByUrl(location.pathname, permissions)

  const permission =
    !pID || (pID && pID === pagePermission?.id)
      ? pagePermission
      : pFindPermissionById(pID, pagePermission?.children || [])
  const name: string | undefined = pID ? pFindPermissionById(pID, pagePermission?.children || [])?.name : undefined

  const findPermissionByCode = (permissionCode: string): PermissionItemProps | undefined => {
    return pFindPermissionByCode(permissionCode, permissions) as PermissionItemProps | undefined
  }

  const findPermissionById = (permissionId: number): PermissionItemProps | undefined => {
    return pFindPermissionById(permissionId, permissions) as PermissionItemProps | undefined
  }

  const findPermissionByUrl = (url: string): PermissionItemProps | undefined => {
    return pFindPermissionByUrl(url, permissions) as PermissionItemProps | undefined
  }
  const checkPermission = (permissionContext?: string, type?: 'ALL' | 'PAGE'): boolean => {
    const _permissions = type === 'ALL' ? permissions : pagePermission?.children || []
    const pContext = permissionContext || permission?.permissionContext
    const p1 = pContext
      ? pagePermission?.permissionContext == pContext
        ? pagePermission
        : undefined || pFindPermissionByCode(pContext, _permissions)
      : undefined
    return !pContext || p1 ? true : false
  }

  const getId = (permissionContext?: string, type?: 'ALL' | 'PAGE'): number | undefined => {
    const _permissions = type === 'ALL' ? permissions : pagePermission?.children || []
    const pContext = permissionContext || permission?.permissionContext
    const p1 = pContext
      ? pagePermission?.permissionContext == pContext
        ? pagePermission
        : undefined || pFindPermissionByCode(pContext, _permissions)
      : undefined
    return p1?.id
  }

  const getUrl = (permissionContext?: string, type?: 'ALL' | 'PAGE'): string | undefined => {
    const _permissions = type === 'ALL' ? permissions : pagePermission?.children || []
    const pContext = permissionContext || permission?.permissionContext
    const p = pContext
      ? pagePermission?.permissionContext == pContext
        ? pagePermission
        : undefined || pFindPermissionByCode(pContext, _permissions)
      : undefined
    const parent = p?.idParent ? findPermissionById(p.idParent) : undefined
    return p?.url || parent?.url
  }

  const getPagePermission = () => {
    return pagePermission
  }

  return {
    activeKey: permission?.permissionContext,
    findPermissionByCode,
    findPermissionById,
    findPermissionByUrl,
    checkPermission,
    getId,
    getUrl,
    getPagePermission,
    name,
    allowAccess: Boolean(permission)
  }
}

export default usePermission
