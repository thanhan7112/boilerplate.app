import { lazy } from 'react'
import ReactDOM from 'react-dom/client'
import vi_VN from 'antd/locale/vi_VN'
import '~/styles/index.scss'
import '~/styles/style-default.scss'
import { Provider } from 'react-redux'
import store from '~/store/index'
import { ConfigProvider, Empty } from 'antd'
import { BrowserRouter } from 'react-router-dom'
import { AppBoundary } from './lib/core/component/app-boundary/index.tsx'

// eslint-disable-next-line react-refresh/only-export-components
const Layout = lazy(() => import(`./layout`))

// eslint-disable-next-line react-refresh/only-export-components
const RouterRender = () => {
  return <Layout />
}

ReactDOM.createRoot(document.getElementById('root')!).render(
  <AppBoundary>
    <Provider store={store}>
      <ConfigProvider
        locale={vi_VN}
        renderEmpty={() => <Empty description='Không có dữ liệu' />}
        getPopupContainer={() => document.getElementById('layout') || document.body}
      >
        <BrowserRouter>
          <RouterRender />
        </BrowserRouter>
      </ConfigProvider>
    </Provider>
  </AppBoundary>
)
