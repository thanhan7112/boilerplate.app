import { ReactNode } from 'react'

export interface FCContentProps {
  pageTitle?: ReactNode
  children: ReactNode
  error?: boolean
  hidePageTitle?: boolean
  className?: string
}

export interface ResponseMenuType {
  id: number
  title: string
  url: string
  children: ResponseMenuType[]
}

export interface ResponseType {
  status: number
  success: boolean
  message: string
  result: any
}

export interface ResponseFileType {
  id: number
  fileName: string
  filePath: string
  fileSize: number
  fileType: string
}

export interface ResponseArticleType {
  id: number
  title: string
  content: string
  menu: ResponseMenuType
}

export interface PermissionItemProps {
  id: number
  name?: string
  permission?: string
  note?: string
  url?: string
  isEnable?: 0 | 1
  sort: number
  idParent?: number
  type?: number
  icon?: string
  htmlId?: string
  menuTop?: boolean
  idChucNang?: string
  permissionContext?: string
  parent?: any
  children: PermissionItemProps[]
  content: string | null
}

export interface FCSiderProps {
  collapsed?: boolean
}

export interface FCRenderComponentGroupProps {
  staticLink: boolean
  path: string
  temp?: boolean
  fallback?: ReactNode
  componentPath?: string
  pageTitle?: string
}
