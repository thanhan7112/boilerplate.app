import heThongRouters from "./he-thong";

const routers: RouteProps[] = [
  ...heThongRouters
];
export interface RouteProps {
  /**
   * 	Quyền hiển thị trang
   */
  pagePermission: string;
  /**
   * 	Đường dẫn tuyệt đối đến page
   */
  componentPath: string;
  temp?: boolean;

  bodyClassName?: string;
}
export default routers;
