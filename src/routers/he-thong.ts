import { RouteProps } from "./index";
import { PERMISSION as PERMISSION_HETHONG_NGUOIDUNG } from "~/pages/co-quan";

const routers: RouteProps[] = [
  {
    pagePermission: PERMISSION_HETHONG_NGUOIDUNG.DANHSACH,
    componentPath: "pages/co-quan",
  },
];
export default routers;
