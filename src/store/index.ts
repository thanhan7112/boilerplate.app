import { configureStore } from '@reduxjs/toolkit'
import reducer from './reducers/default'

const store = configureStore({
  reducer: reducer
})

export default store

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>

// Inferred type: {users: UsersState}
export type AppDispatch = typeof store.dispatch
