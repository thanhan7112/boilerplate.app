import { PermissionItemProps, ResponseMenuType } from '~/model'

// Định nghĩa kiểu cho action
interface Action<T = string, P = any> {
  type: T
  payload?: P
}

// Kiểu riêng cho action 'GET_MENU'
interface SetMenuAction extends Action<'SET_MENU', { menu: ResponseMenuType[] }> {}
interface SetPermissionAction extends Action<'SET_PERMISSION', { permission: PermissionItemProps[] }> {}
interface SetMenuLoadingAction extends Action<'SET_MENU_LOADING', { isLoading: boolean }> {}
interface SetContentAction extends Action<'SET_CONTENT', { content: string }> {}

// Tạo Union type cho tất cả các action trong reducer này
type ReducerActions = SetMenuAction | SetMenuLoadingAction | SetPermissionAction | SetContentAction

export interface TypeRootReducer {
  menu: ResponseMenuType[]
  menuLoading: boolean
  permission: PermissionItemProps[]
  content: string | null
}

const initialStates: TypeRootReducer = {
  menu: [],
  menuLoading: false,
  permission: [],
  content: null
}

const reducer = (state = initialStates, action: ReducerActions): TypeRootReducer => {
  const type = action.type
  switch (type) {
    case 'SET_MENU':
      const list = action.payload?.menu ?? []
      return {
        ...state,
        menu: list
      }
    case 'SET_MENU_LOADING':
      const isLoading = action.payload?.isLoading === true
      return {
        ...state,
        menuLoading: isLoading
      }
    case 'SET_PERMISSION':
      const permissionUpdate = action.payload?.permission as PermissionItemProps[]
      return {
        ...state,
        permission: [...state.permission, ...permissionUpdate]
      }
    case 'SET_CONTENT':
      const content = action.payload?.content ?? null
      return {
        ...state,
        content: content
      }
    default:
      return state
  }
}

export default reducer
