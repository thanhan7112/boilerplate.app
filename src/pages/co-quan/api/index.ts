
export interface ItemProps {
    id: number,
    tenCoQuan: string,
    kyHieu: string,
    maDinhDanh: string,
    coQuanChaId: number,
    maCoQuanCha: string,
    thuTu: number,
    capCoQuanId: number,
    matKhau: string,
    soDienThoai: string,
    diaChi: string,
    logoCoQuan: string,
    maPhuong: string,
    maHuyen: string,
    gioHenTraThuTucTrongNgay: string,
    maDonViHanhChinh: string,
    nguoiQuanTriId: number,
    tenMien: string
}
