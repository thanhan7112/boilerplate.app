//The document is automatically generated by tool
import React, { Fragment, lazy, Suspense } from "react";
import { PERMISSION } from "../index";
import usePermission from "~/lib/hooks/usePermission";
import LoadingSpinner from "~/lib/load/lds-spinner";

const List = lazy(() => import(`./list`));
const Create = lazy(() => import(`./create`));
const Edit = lazy(() => import(`./edit`));

const CoQuan = () => {
    const permission = usePermission();
    return <Fragment>
        <Suspense fallback={<LoadingSpinner />}>
            {permission.activeKey === PERMISSION.DANHSACH && <List />}
			{permission.activeKey === PERMISSION.THEMMOI &&  <Create />}
			{permission.activeKey === PERMISSION.CAPNHAT &&  <Edit />}
        </Suspense>
    </Fragment>
}

export default CoQuan;
