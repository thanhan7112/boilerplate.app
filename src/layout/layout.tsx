/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react'
import Header from './header/index'
import { Route, Routes } from 'react-router-dom'
import { PageError404 } from './index'
import routers from '~/routers'
import RenderComponent from './render-component'
import { findPermissionByCode } from '~/utils'
import { Layout } from 'antd'
import Sider from './sider'
import Home from './home'
import { useDispatch, useSelector } from 'react-redux'
import { TypeRootReducer } from '~/store/reducers/default'
import axios from 'axios'
import { HOST_NAME } from '~/utils/api-endpoint'
import { ResponseType } from '~/model'

const Main = () => {
  const permissions = useSelector((state: TypeRootReducer) => state.permission)
  const dispatch = useDispatch()

  useEffect(() => {
    ;(async () => {
      try {
        const res: ResponseType = (await axios.get(`${HOST_NAME}/api/menu`)).data
        if (res.success) {
          dispatch({
            type: 'SET_PERMISSION',
            payload: {
              permission: res.result
            }
          })
        }
      } catch (err) {
        return
      }
    })()
  }, [])

  const routesPermission = routers
    .filter((item) => {
      const p = findPermissionByCode(item.pagePermission, permissions)
      return Boolean(p && p.url)
    })
    .map((item) => {
      const p = findPermissionByCode(item.pagePermission, permissions)
      return {
        static: false,
        path: p?.url as string,
        componentPath: item.componentPath,
        temp: item?.temp === false ? false : true,
        pageTitle: p?.name ?? ''
      }
    })

  return (
    <Layout style={{ height: '100vh - 56px' }}>
      <Header />
      <Layout className='style-layout'>
        <Sider />
        <Layout.Content id='wrapper-layout'>
          <div className='relative scroll-y'>
            <Routes>
              {routesPermission.map((item, i) => (
                <Route
                  path={item.path}
                  key={i}
                  element={
                    <RenderComponent
                      staticLink={item.static}
                      pageTitle={item?.pageTitle}
                      path={item.path}
                      key={item.componentPath}
                      temp={item.temp}
                      componentPath={item.componentPath}
                    />
                  }
                />
              ))}
              <Route path={'/'} element={<Home />} />
              <Route path='*' element={<PageError404 />} />
            </Routes>
          </div>
        </Layout.Content>
      </Layout>
    </Layout>
  )
}

export default Main
