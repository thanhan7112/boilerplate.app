import { Layout, Menu } from 'antd'
import { FC, Fragment, useCallback, useEffect, useMemo, useState } from 'react'
import { Link } from 'react-router-dom'
import usePermission from '~/lib/hooks/usePermission'
import { FCSiderProps, PermissionItemProps } from '~/model'
import './style.scss'
import { TypeRootReducer } from '~/store/reducers/default'
import CommonScrollBar from '~/lib/scroll-bar'
import useLocalData from '~/lib/hooks/useLocalData'
import { useSelector } from 'react-redux'

const Sider: FC<FCSiderProps> = () => {
  const localData = useLocalData()
  const [collapsed, setCollapsed] = useState<boolean>(localData.menu.getCollapsed())
  const [openKeys, setOpenKeys] = useState<string[]>(localData.menu.getOpenKeys())
  const permissions = useSelector((state: TypeRootReducer) => state.permission)
  const p = usePermission()

  const permissionId = p.getPagePermission()?.id

  useEffect(() => {
    document.getElementById('siderbar-scroll')?.scrollTo(0, localData.menu.getScrollTop())
  }, [])

  const renderMenu = useCallback(
    (permissions: PermissionItemProps[]) => {
      return permissions.map((item) => {
        if (item.children.length) {
          return (
            <Menu.SubMenu
              title={<span className='menu-name'>{item.name}</span>}
              icon={<i className={item.icon || 'fa fa-angle-right'} />}
              key={`${item.id}`}
              {...{ ['id']: `menu-${item.id}` }}
            >
              {renderMenu(item.children || [])}
            </Menu.SubMenu>
          )
        } else {
          return (
            <Menu.Item
              icon={<i className={item.icon || 'fa fa-angle-right'} />}
              key={`${item.id}`}
              {...{ ['id']: `menu-${item.id}` }}
            >
              <Link
                className='menu-name'
                to={`${item.url}?id=${item.id}` || ''}
                onClick={() => {
                  document.body.classList.remove('show-menu')
                }}
              >
                {item.name}
              </Link>
            </Menu.Item>
          )
        }
      })
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [permissionId, collapsed]
  )

  const result = useMemo(() => {
    return (
      <Layout.Sider
        style={{ background: 'none' }}
        width={300}
        className='layout-sider-bar'
        collapsible
        onCollapse={(collapsed) => {
          setCollapsed(collapsed)
          localData.menu.setCollapsed(collapsed)
        }}
        collapsed={collapsed}
        trigger={collapsed ? <i className='fa fa-angle-double-right' /> : <i className='fa fa-angle-double-left' />}
        id='sidebar-desktop'
      >
        <CommonScrollBar
          id='siderbar-scroll'
          onScroll={(e) => localData.menu.setScrollTop(e.currentTarget.scrollTop)}
          height='calc(100vh - 56px)'
        >
          <div style={{ minHeight: 'calc(100vh - 56px)', overflow: 'hidden' }}>
            <Menu
              key={'menu-mobiles'}
              className='main-sider'
              onOpenChange={(_openKeys) => {
                const keys = _openKeys as string[]
                const keyChanged = keys.find((k) => !openKeys.includes(k)) || openKeys.find((k) => !keys.includes(k))
                const open = Boolean(keys.find((k) => !openKeys.includes(k)))

                let newOpenKeys: string[] = []
                if (permissions.findIndex((p) => p.id.toString() === keyChanged) === -1) {
                  newOpenKeys = keys
                } else {
                  newOpenKeys = [
                    ...keys.filter((k) => permissions.findIndex((p) => p.id.toString() === k) === -1),
                    ...(open && keyChanged ? [keyChanged] : [])
                  ]
                }
                setOpenKeys(newOpenKeys)
                !collapsed && localData.menu.setOpenKeys(newOpenKeys)
              }}
              defaultOpenKeys={openKeys}
              openKeys={openKeys}
              mode='inline'
              selectedKeys={permissionId ? [`${permissionId}`] : []}
              style={{ flex: 1, borderRight: 'none', minHeight: 'calc(100vh - 56px)' }}
            >
              {renderMenu(permissions)}
            </Menu>
          </div>
        </CommonScrollBar>
      </Layout.Sider>
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [permissions, permissionId, collapsed, openKeys])
  return <Fragment>{result}</Fragment>
}

export default Sider
