import { FC } from "react";
import './style.scss'

interface PageHeaderProps {
  style?: React.CSSProperties;
  className?: string;
  backIcon?: React.ReactNode;
  title?: React.ReactNode;
  onBack?: () => void;
  subTitle?: React.ReactNode;
  error?: boolean;
  hidePageTitle?: boolean;
  pageTitle?: React.ReactNode;
  children?: React.ReactNode;
}

const PageHeader: FC<PageHeaderProps> = ({
  // style,
  className,
  backIcon,
  title,
  onBack,
  subTitle,
  error,
  hidePageTitle,
  pageTitle,
  // children,
}) => (
  <div className={`${!error ? "layout-page-header" : ""} ${className}`}>
    {!hidePageTitle && !error && (
      <div className="flex flex-row gap-sm">
        {backIcon && (
          <span onClick={onBack} style={{ cursor: "pointer" }}>
            {backIcon}
          </span>
        )}
        <span>{title || pageTitle}</span>
        {subTitle && <span>{subTitle}</span>}
      </div>
    )}
    {/* {children} */}
  </div>
);

export default PageHeader;
