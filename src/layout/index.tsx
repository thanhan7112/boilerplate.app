import Layout from './layout'
import { PageError404 } from '~/lib/core/component/app-boundary/page/not-found'

export { PageError404 }
export default Layout
