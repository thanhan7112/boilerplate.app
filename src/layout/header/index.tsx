import { Link } from 'react-router-dom'
import './style.scss'

const Header = () => {
  return (
    <div className='flex flex-al-center flex-row gap-sm pd-tiny header'>
      <div className='header-inner'>
        <Link className='flex flex-al-center flex-row gap-sm' to={'/'}>
          <img src='/vite.svg' alt='Library' />
          <h1 className='logo-title'>Storybook</h1>
        </Link>
      </div>
    </div>
  )
}

export default Header
