import React, { FC, Fragment } from 'react';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { useLocation, useNavigate } from 'react-router-dom';
import { parse } from 'qs';
import { FCContentProps } from '~/model';
import usePermission from '~/lib/hooks/usePermission';
import PageHeader from './page-header';
import './style.scss'

const Content: FC<FCContentProps> = ({ children, error, pageTitle, hidePageTitle, className }) => {
	const history = useNavigate();
	const p = usePermission();
	const location = useLocation();
	const qs = parse(location.search, { ignoreQueryPrefix: true });
	const title = qs.title as (string | undefined);
	const subTitle = qs.sub_title as (string | undefined);

	return <Fragment>
		<PageHeader
			error={error}
			backIcon={!hidePageTitle && !error && <ArrowLeftOutlined />}
			title={!hidePageTitle && !error && (title || pageTitle || p.getPagePermission()?.name)}
			onBack={() => history(-1)}
			subTitle={!hidePageTitle && !error && (subTitle || p.name)}
		>
			<div className={className}>
				{children}
			</div>
		</PageHeader>
		{/* <div className={`${className} content-container`}>
			{children}
		</div> */}
	</Fragment>
}

export default Content;