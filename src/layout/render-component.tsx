import React, { FC, lazy, Suspense, useEffect, useMemo, useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { parse, stringify } from 'qs';
import Content from './content';
import { PageError404 } from '.';
import LoadingSpinner from '~/lib/load/lds-spinner';
import { FCRenderComponentGroupProps } from '~/model';
import usePermission from '~/lib/hooks/usePermission';

const RenderComponent: FC<FCRenderComponentGroupProps> = ({
  pageTitle,
  componentPath,
}) => {
  const p = usePermission();
  const location = useLocation();
  const navigate = useNavigate(); // Using useNavigate instead of Redirect

  const qs = parse(location.search, { ignoreQueryPrefix: true });

  useEffect(() => {
    document.getElementById("layout")?.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    const pageTitle = p.getPagePermission()?.name ? ` | ${p.getPagePermission()?.name}` : "";
    const actionTitle = p?.name ? ` | ${p?.name}` : "";
    document.title = `${pageTitle}${actionTitle}`;
  }, [p?.name, p.getPagePermission()?.name]);

  const pRedirect = qs.redirect_to && p.findPermissionByCode(qs.redirect_to as string);
  if (qs.redirect_to) {
    if (!pRedirect || !p.allowAccess) {
      return <PageError404 />;
    } else {
      navigate(pRedirect.url + decodeURIComponent(stringify({
        ...qs,
        p: pRedirect.id,
        p_redirect_from: qs.p,
        redirect_to: undefined
      })));
    }
  }

  const LazyComponent = useMemo(() => {
    return lazy(() => import(`../${componentPath}`).catch(() => import("~/lib/core/component/app-boundary/page/not-found")));
  }, [componentPath]);

  if (componentPath) {
    try {
      return (
        <Suspense fallback={<LoadingSpinner />}>
          <Content pageTitle={pageTitle}>
            <LazyComponent />
          </Content>
        </Suspense>
      );
    } catch (e) {
      return <PageError404 />;
    }
  }
  return <PageError404 />;
}

export default RenderComponent;
