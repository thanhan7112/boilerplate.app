import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react-swc';
import path from 'path';

export default defineConfig({
    plugins: [react()],
    server: {
        port: 7643,
        host: 'localhost',
    },
    css: {
        devSourcemap: true,
    },
    resolve: {
        alias: {
            '~': path.resolve(__dirname, './src')
        }
    },
    optimizeDeps: {
        esbuildOptions: { target: "esnext" } 
    },
});
